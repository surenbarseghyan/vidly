﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class MoviesController : ApiController
    {
        ApplicationDbContext _context;
        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }
        //GET api/Movies
        public IHttpActionResult GetMovies(string query=null)
        {
            var moviesQuery = _context.Movies
                .Include(m => m.Genre)
                .Where(m=>m.AvailibleNumber>0);
            if (!String.IsNullOrWhiteSpace(query)) 
            {
                moviesQuery = moviesQuery.Where(m=>m.Name.Contains(query));
            }

           var movies =  moviesQuery.ToList()
                .Select(Mapper.Map<Movie, MovieDto>);
            return Ok(movies);
        }
        //GET api/Movies/1
        public IHttpActionResult GetMovie(int id)
        {
            var Movie = _context.Movies.SingleOrDefault(c => c.Id == id);
            if (Movie == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<Movie, MovieDto>(Movie));
        }
        [Authorize(Roles=Roles.CanManageMovies)]
        //POST api/Movies/
        [HttpPost]
        public IHttpActionResult CreateMovie(MovieDto MovieDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var Movie = Mapper.Map<MovieDto, Movie>(MovieDto);
            _context.Movies.Add(Movie);
            _context.SaveChanges();
            return Created(new Uri(Request.RequestUri + "/" + Movie.Id), MovieDto);
        }
        [Authorize(Roles = Roles.CanManageMovies)]
        //PUT api/Movies/1
        [HttpPut]
        public IHttpActionResult UpdateMovie(int id, MovieDto MovieDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var MovieInDb = _context.Movies.SingleOrDefault(c => c.Id == id);
            if (MovieInDb == null)
            {
                return NotFound();
            }
            Mapper.Map(MovieDto, MovieInDb);
            _context.SaveChanges();
            return Ok(MovieDto);
        }
        [Authorize(Roles = Roles.CanManageMovies)]
        //DELETE api/Movies/1
        [HttpDelete]
        public IHttpActionResult DeleteMovie(int id)
        {
            var Movie = _context.Movies.SingleOrDefault(c => c.Id == id);
            if (Movie == null)
            {
                return NotFound();
            }
            _context.Movies.Remove(Movie);
            _context.SaveChanges();
            return Ok();
        }

    }
}
