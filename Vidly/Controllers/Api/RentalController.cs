﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class RentalController : ApiController
    {
        ApplicationDbContext _context;
        public RentalController()
        {
            _context = new ApplicationDbContext();
        }
        //Post api/rental
        [HttpPost]
        public IHttpActionResult NewRental(RentalDto rentalDto) 
        {
            var customer = _context.Customers.Single(c=>c.Id == rentalDto.CustomerId);
            List<Rental> rentals = new List<Rental>();
            var movies = _context.Movies.Where(m=>rentalDto.MovieIds.Contains(m.Id));
            foreach (var movie in movies)
            {
                if (movie.AvailibleNumber == 0)
                    return BadRequest("Movie isn't availible");
                movie.AvailibleNumber--;
                Rental rental = new Rental
                {
                    Movie = movie,
                    Customer = customer
                };
                _context.Rentals.Add(rental);
                rentals.Add(rental);
            }
            _context.SaveChanges();
            return Ok(rentals);
        }
    }
}
