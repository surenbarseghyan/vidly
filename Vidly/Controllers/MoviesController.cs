﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        private ApplicationDbContext _context;
        public MoviesController()
        {
            _context = new ApplicationDbContext();   
        }
        protected override void Dispose(bool disposing)
        {
           _context.Dispose();
        }
        public ActionResult Index() 
        {
            if (User.IsInRole(Roles.CanManageMovies))
                return View("List");
            else
                return View("GuestList");
        }
        
        public ActionResult Details(int? id) 
        {
            Movie movie = _context.Movies.Include("Genre").Where(m=>m.Id==id).FirstOrDefault();
            if (id.HasValue && movie!=null) 
            {
                return View(movie);
            }
            else
            {
                return HttpNotFound();
            }
        }
        [Authorize(Roles = Roles.CanManageMovies)]
        public ActionResult New() 
        {
            MovieFormViewModel viewModel = new MovieFormViewModel
            {
                Genres = _context.Genres,
                Id = 0,
            };
            return View("MovieForm",viewModel);
        }
        [Authorize(Roles = Roles.CanManageMovies)]
        public ActionResult Edit(int id) 
        {
            var movie = _context.Movies.SingleOrDefault(m => m.Id == id);
            MovieFormViewModel viewModel = new MovieFormViewModel(movie)
            {
                Genres = _context.Genres,
            };
            return View("MovieForm", viewModel);
        }

        [HttpPost]
        public ActionResult Save(Movie movie) 
        {
            if(movie.Id==0)
            _context.Movies.Add(movie);
            else 
            {
                var movieInDb = _context.Movies.Single(m=>m.Id ==movie.Id);
                movieInDb.Name = movie.Name;
                movieInDb.ReleasedTime = movie.ReleasedTime;
                movieInDb.GenreId = movie.GenreId;
                movieInDb.NumberInStock = movie.NumberInStock;
            }
            _context.SaveChanges();
            return RedirectToAction("Index","Movies");
        }

    }
}