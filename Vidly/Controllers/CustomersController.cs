﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;
        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Customers
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Details(int id)
        {
            Customer model = _context.Customers.Include(c=>c.MemberShipType).Where(m => m.Id == id).FirstOrDefault();
            if (model == null) 
            {
                return HttpNotFound();
            }
            else 
            {
                return View(model);
            }
        }
        public ActionResult New()
        {
            CustomerViewModel newCustomerViewModel = new CustomerViewModel
            {
                MemberShipTypes = _context.MemberShipTypes.ToList(),
                Customer = new Customer()
            };
            return View("CustomerForm",newCustomerViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer) 
        {
            if (!ModelState.IsValid) 
            {
                CustomerViewModel viewModel = new CustomerViewModel
                {
                    Customer = customer,
                    MemberShipTypes = _context.MemberShipTypes    
                };
                return View("CustomerForm", viewModel);
            }
            if(customer.Id == 0) 
            {
                _context.Customers.Add(customer);

            }
            else 
            {
                var customerInDb = _context.Customers.Single(c=>c.Id == customer.Id);
                customerInDb.Name = customer.Name;
                customerInDb.BirthDate = customer.BirthDate;
                customerInDb.MemberShipTypeId = customer.MemberShipTypeId;
                customerInDb.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Customers");
        }
        public ActionResult Edit(int id) 
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null)
                return HttpNotFound();
            else
            {
               CustomerViewModel viewModel = new CustomerViewModel
                {
                   Customer =customer,
                   MemberShipTypes = _context.MemberShipTypes
                };
                return View("CustomerForm",viewModel); 

            }



        }
    }
}