namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAvailibleNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "AvailibleNumber", c => c.Int(nullable: false));
            Sql("UPDATE Movies SET AvailibleNumber=NumberInStock");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Movies", "AvailibleNumber");
        }
    }
}
