namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserRoles : DbMigration
    {
        public override void Up()
        {
            Sql(@"
               INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'07d3b42f-51be-43f5-8b32-6aeed5909134', N'admin@vidly.com', 0, N'ACEvMDKY8hA30pSvpIwCqHK7IEHnwxrtPstZ4BpZeu+ctn/ggBedCPrZ2iI6vHbJXA==', N'c809fa6e-b792-4c2e-9b35-b34fb1ac57ef', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
               INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'aedda710-c4cb-42f5-948b-d8d86ce11073', N'guest@vidly.com', 0, N'AB+ezeyZfM8drgHV+jMKyidlZq7Kp9DF4cvVK6sagOTa3tQv6Jt4UkJRn7Xj8htg1Q==', N'726eb4d5-1a91-400f-a7c5-09a95037b349', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
               INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'5a995831-452a-4e0b-93bc-16118ad7f4e6', N'CanManageMovies')
               INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'07d3b42f-51be-43f5-8b32-6aeed5909134', N'5a995831-452a-4e0b-93bc-16118ad7f4e6')
     
            ");
        }
        
        public override void Down()
        {
        }
    }
}
