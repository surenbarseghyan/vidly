﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class CustomerDto
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        public bool IsSubscribedToNewsLetter { get; set; }
        public MemberShipTypeDto MemberShipType { get; set; }
        public byte MemberShipTypeId { get; set; }
        //[Min18AgeIfAMember]
        public DateTime? BirthDate { get; set; }
    }
}