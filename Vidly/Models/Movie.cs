﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [Display(Name ="Release Date")]
        public DateTime ReleasedTime { get; set; }
        [Required]
        public DateTime AddedTime { get; set; } = DateTime.Now;

        public Genre Genre { get; set; }

        
        [Display(Name = "Genre Type")]
        public int GenreId { get; set; }

        [Required]
        [Range(1,20)]
        [Display(Name="Number in Stock")]
        public int NumberInStock { get; set; }
        public int AvailibleNumber { get; set; }


    }
}