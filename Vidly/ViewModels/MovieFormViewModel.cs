﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class MovieFormViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }
        public int? Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Release Date")]
        public DateTime? ReleasedTime { get; set; }

        [Display(Name = "Genre Type")]
        [Required]
        public int? GenreId { get; set; }

        [Required]
        [Range(1, 20)]
        [Display(Name = "Number in Stock")]
        public int? NumberInStock { get; set; }

        public string Title 
        { 
            get 
            {
                return (Id != 0)?"Edit Movie":"Add Movie"; 
            }
            
        }
        public MovieFormViewModel()
        {

        }
        public MovieFormViewModel(Movie movie)
        {
            Name = movie.Name;
            ReleasedTime = movie.ReleasedTime;
            GenreId = movie.GenreId;
            NumberInStock = movie.NumberInStock;
        }
    }
}